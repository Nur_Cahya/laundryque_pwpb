<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paket extends Model
{
    protected $table ='paket';
	
	protected $primaryKey ='id_paket';

    protected $fillable = [
    	'id_outlet','jenis','nama_paket','harga'
    ];


    public function outlet(){
    	return $this->belongsTo('App\Outlet');
    }



}
