<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outlet extends Model
{
	protected $table ='outlet';
	
	protected $primaryKey ='id_outlet';

    protected $fillable = [
    	'nama_outlet','alamat','no_telp'
    ];

    public function paket(){
    	return $this->hasMany('App\paket','id_outlet');
    }

    public function transaksi(){
    	return $this->hasMany('App\paket','id_outlet');
    }
    

}
