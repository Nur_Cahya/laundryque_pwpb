<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(request $request){

        $input = $request->all();

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (auth()->attempt(array('email' => $input['email'], 'password' => $input['password']))) {
            if (auth()->user()->isAdmin == 1) {
                return redirect()->route('outlets.index');
            }
            else if (auth()->user()->isKasir == 1){
                return redirect()->route('transaksi.index');
            }
            else if (auth()->user()->isOwner == 1){
                return redirect()->route('home');
            }
            else if (auth()->user()->isKasir == 0 && auth()->user()->isAdmin == 0){
                return redirect()->route('home');
            }
        }else{
                return redirect()->route('login');
        }   

    }

}  
