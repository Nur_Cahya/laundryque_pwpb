<?php

namespace App\Http\Controllers;

use App\Outlet;
use App\paket;
use Illuminate\Http\Request;

class OutletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $outlets = Outlet::latest()->paginate(5);

        return view('outlet.index',compact('outlets'))
            ->with('i',(request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('outlet.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_outlet' => 'required',

            'alamat' => 'required',

            'no_telp' => 'required',
        ]);

        Outlet::create($request->all());

        return redirect()->route('outlets.index')
                        ->with('success','Outlet Telah Ditambahkan !.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Outlet  $outlet
     * @return \Illuminate\Http\Response
     */
    public function show(Outlet $outlet) 
    {
        $paket = paket::get();
        return view('outlet.show',compact('outlet','paket'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Outlet  $outlet
     * @return \Illuminate\Http\Response
     */
    public function edit(Outlet $outlet)
    {
         return view('outlet.edit',compact('outlet'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Outlet  $outlet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Outlet $outlet)
    {
        $request->validate([
            'nama_outlet' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
        ]);

        $outlet->update($request->all());

        return redirect()->route('outlets.index')
                        ->with('success','Outlet Berhasil Diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Outlet  $outlet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Outlet $outlet)
    {
        $outlet->delete();

        return redirect()->route('outlets.index')
                        ->with('success','Outlet Berhasil di Delete');
    }
}
