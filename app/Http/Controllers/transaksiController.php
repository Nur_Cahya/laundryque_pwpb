<?php

namespace App\Http\Controllers;

use App\transaksi;
use App\member;
use App\Outlet;
use App\User;
use App\paket;

use Illuminate\Http\Request;
use Carbon\carbon;

class transaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksi = transaksi::latest()->paginate(10);
        return view('transaksi.index',compact('transaksi'))
            ->with('i',(request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $date = Carbon::now();
        $tgl = $date;
        $status = 'baru';
        $user = auth()->user()->id;
        $outlet = Outlet::get();
        $invoice = transaksi::Invoice();
        $member = member::get();

        $paket = paket::get();
        return view('transaksi.create',compact('tgl','user','invoice','member','outlet','paket','status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

         transaksi::create([
            'id_user'     => $data['id_user'],
            'id_outlet'   => $data['id_outlet'],
            'kode_invoice' => $data['kode_invoice'], 
            'tgl'         => $data['tgl'],
            'status'         => $data['status'],
            'id_member'         => $data['id_member'],
            'dibayar'         => $data['dibayar'],
            'id_paket'         => $data['id_paket'],
        ]);
        return redirect()->route('transaksi.index')->with('success', 'Data Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function show(transaksi $transaksi)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function edit(transaksi $transaksi)
    {
        $date = Carbon::now();
        $tgl = $date;
        $status = 'baru';
        $user = auth()->user()->id;
        $outlet = Outlet::get();
        $invoice = transaksi::Invoice();
        $member = member::get();

        $paket = paket::get();
        return view('transaksi.edit',compact('tgl','user','invoice','member','outlet','paket','status','transaksi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, transaksi $transaksi)
    {
        $data = $request->all();
        $id =  $transaksi->id_transaksi;
         transaksi::where('id_transaksi', $id)->update([
            'status'         => $data['status'],
            'dibayar'         => $data['dibayar'],
        ]);
         return redirect()->route('transaksi.index')->with('success', 'Data Added successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function destroy(transaksi $transaksi)
    {
        $transaksi->delete();

        return redirect()->route('transaksi.index')
                        ->with('success','transaksi Berhasil di Delete');
    }
}
