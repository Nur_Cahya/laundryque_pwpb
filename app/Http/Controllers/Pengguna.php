<?php

namespace App\Http\Controllers;

use App\User;
use App\Outlet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class Pengguna extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::latest()->paginate(10);
        return view('pengguna.index', compact('user'))
                ->with('i',(request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $outlet = Outlet::get();
        return view('auth.register',compact('outlet'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_user' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'role'  => 'required',
            'id_outlet' => 'required',
        ]);

        if ($request->role == 'admin') {
            $create = array(
                'nama_user' => $request->nama_user,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'id_outlet' => $request->id_outlet,            
                'isAdmin' => '1',
            );

                User::create($create);
        }

        else if($request->role == 'kasir'){
            $create = array(
                'nama_user' => $request->nama_user,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'id_outlet' => $request->id_outlet,           
                'isKasir' => '1',
            );
                 User::create($create);
        }

        else if($request->role == 'owner'){
                $create = array(
                'nama_user' => $request->nama_user,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'id_outlet' => $request->id_outlet,            
                'isOwner' => '1',
            );
                User::create($create);
        }
        else{
            return redirect()->route('register');
        }

        
        return redirect()->route('pengguna.index')
        ->with('succes','user anda sudah berhasil ditambahkan');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findorfail($id);
        $outlet = Outlet::get();
        return view('pengguna.edit',compact('user','outlet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'id' =>'required',
            'nama_user' => 'required',
            'email' => 'required',
            'role' => 'required',
            'id_outlet' => 'required'
        ]);

        if ($request->role == 'admin') {
            $create = array(
                'id' => $request->id,
                'nama_user' => $request->nama_user,
                'email' => $request->email,
                'id_outlet' => $request->id_outlet,            
                'isAdmin' => '1',
            );

                User::whereId($request->id)->update($create);
        }

        else if($request->role == 'kasir'){
            $create = array(
                'id' => $request->id,
                'nama_user' => $request->nama_user,
                'email' => $request->email,
                'id_outlet' => $request->id_outlet,           
                'isKasir' => '1',
            );
                 User::whereId($request->id)->update($create);
        }

        else if($request->role == 'owner'){
                $create = array(
                'id' => $request->id,
                'nama_user' => $request->nama_user,
                'email' => $request->email,
                'id_outlet' => $request->id_outlet,            
                'isOwner' => '1',
            );
                User::whereId($request->id)->update($create);
        }
        else{
            return redirect()->route('register');
        }

        
        return redirect()->route('pengguna.index')
        ->with('succes','user anda sudah berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('pengguna.index')
                        ->with('success','User Berhasil di Delete');    }
}
