<?php

namespace App\Http\Controllers;

use App\Outlet;
use App\paket;
use Illuminate\Http\Request;

class paketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $outlet = Outlet::get();
        return view('paket.create',compact('outlet'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'id_outlet' => 'required',

            'nama_paket' => 'required',

            'jenis' => 'required',
            'harga' => 'required'
        ]);

        paket::create($request->all());
        $id =  $request->id_outlet;
        return redirect()->route('outlets.show',['id_outlet' => $id])
                        ->with('success','Outlet Telah Ditambahkan !.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\paket  $paket
     * @return \Illuminate\Http\Response
     */
    public function show($id_outlet)
    {
        // $paket = paket::findOrFail($id_outlet);
        // return view('paket.show', compact('paket'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\paket  $paket
     * @return \Illuminate\Http\Response
     */
    public function edit(paket $paket)
    {
        return view('paket.edit',compact('paket'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\paket  $paket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, paket $paket)
    {
        $request->validate([

            'nama_paket' => 'required',

            'jenis' => 'required',
            'harga' => 'required'
        ]);

        $paket->update($request->all());

        return redirect()->route('outlets.index')
                        ->with('success','Paket Berhasil Diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\paket  $paket
     * @return \Illuminate\Http\Response
     */
    public function destroy(paket $paket)
    {

        $paket->delete();

        $id =  $paket->id_paket;
        return redirect()->route('outlets.show',['id_outlet' => $id])
                        ->with('success','Outlet Berhasil di Delete');
    }
}
