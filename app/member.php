<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class member extends Model
{
    protected $table ='member';
	
	protected $primaryKey ='id_member';

    protected $fillable = [
    	'nama','alamat','no_telp','jk'
    ];

}
 