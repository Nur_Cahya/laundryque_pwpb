<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{ 
    protected $table ='transaksi';
	
	protected $primaryKey ='id_transaksi';

    protected $fillable = [
    	'id_outlet','kode_invoice','id_member','tgl','status','dibayar','id_user','id_paket'
    ];

    public function user(){
    	return $this->belongsTo('App\User','id_user');
    }
    public function outlet(){
    	return $this->belongsTo('App\Outlet','id_outlet');
    }
    public function member(){
    	return $this->belongsTo('App\member','id_member');
    }

     public function paket(){
        return $this->belongsTo('App\paket','id_paket');
    }

    public function scopeInvoice()
    {
        $latest = transaksi::latest()->first();

        if (! $latest) {
            return 'arm0001';
        }

        $string = preg_replace("/[^0-9\.]/", '', $latest->kode_invoice);

        return 'arm' . sprintf('%04d', $string+1);
    }

}
