<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        $user =[
                [
                 'nama_user' => 'admin',
                 'email' => 'admin123@gmail.com',
                 'password' => bcrypt('123456789'),
                 'isAdmin' => '1',
                 'id_outlet' => '1'
                 
                ],
        		
			 	[
        		 'nama_user' => 'kasir',
				 'email' => 'kasir123@gmail.com',
				 'password' => bcrypt('123456789'),
				 'isKasir' => '1',
                 'id_outlet' => '1'
				 
			 	],
				[
        		 'nama_user' => 'owner',
				 'email' => 'owner123@gmail.com',
				 'password' => bcrypt('123456789'),
				 'id_outlet' => '1',
                 'isOwner' => '1',
				 
			 	]
        	];
        	foreach ($user as $key => $value) {
        		User::create($value);
        	}
    }
}
