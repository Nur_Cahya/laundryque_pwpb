<?php

use Illuminate\Database\Seeder;
use App\paket;

class paketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        paket::truncate();
        $paket =[
                [
                 'id_outlet' => '1',
                 'nama_paket' => 'Cuci Celana',
                 'jenis' => 'Celana',
                 'harga' => '3000',
                 
                ],
        		
			 	[
        		 'id_outlet' => '1',
                 'nama_paket' => 'Cuci baju',
                 'jenis' => 'Baju',
                 'harga' => '3000',
				 
			 	],

				[
        		 'id_outlet' => '1',
                 'nama_paket' => 'sweet Dream',
                 'jenis' => 'Selimut',
                 'harga' => '3000',
				 
			 	],

			 	[
                 'id_outlet' => '2',
                 'nama_paket' => 'Cuci Celana',
                 'jenis' => 'Celana',
                 'harga' => '3000',
                 
                ],
        		
			 	[
        		 'id_outlet' => '2',
                 'nama_paket' => 'Cuci baju',
                 'jenis' => 'Baju',
                 'harga' => '3000',
				 
			 	],

				[
        		 'id_outlet' => '2',
                 'nama_paket' => 'sweet Dream',
                 'jenis' => 'Selimut',
                 'harga' => '3000',
				 
			 	],

			 	[
                 'id_outlet' => '3',
                 'nama_paket' => 'Cuci Celana',
                 'jenis' => 'Celana',
                 'harga' => '3000',
                 
                ],
        		
			 	[
        		 'id_outlet' => '3',
                 'nama_paket' => 'Cuci baju',
                 'jenis' => 'Baju',
                 'harga' => '3000',
				 
			 	],

				[
        		 'id_outlet' => '3',
                 'nama_paket' => 'sweet Dream',
                 'jenis' => 'Selimut',
                 'harga' => '3000',
				 
			 	],


        	];
        	foreach ($paket as $key => $value) {
        		paket::create($value);
        	}
    }
}
