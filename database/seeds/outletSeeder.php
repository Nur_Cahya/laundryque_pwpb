<?php

use Illuminate\Database\Seeder;
use App\Outlet;

class outletSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Outlet::truncate();
        $outlet =[
                [
                 'nama_outlet' => 'Cuci Ceria',
                 'alamat' => 'Bandung',
                 'no_telp' => '123456789',
                 
                ],
        		
			 	[
        		 'nama_outlet' => 'Sweet Laundry',
                 'alamat' => 'Bandung',
                 'no_telp' => '123456789',
				 
			 	],
				[
        		 'nama_outlet' => 'Laundry mamih Ita',
                 'alamat' => 'Bandung',
                 'no_telp' => '123456789',
				 
			 	]
        	];
        	foreach ($outlet as $key => $value) {
        		Outlet::create($value);
        	}
    }
}

