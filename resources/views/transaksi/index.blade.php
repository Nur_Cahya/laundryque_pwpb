@extends('layouts.app')

@section('content')

<div class="container-fluid">	
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

<div class="row">
	<div class="col-lg-12 margin-tb">
		<div class="pull-left">
			<div class="title">

			<h2>Data transaksi</h2>
		</div>
		<div class="pull-right">
			<a class="btn btn-success" href="{{ route('transaksi.create') }}"> Buat transaksi Baru</a>
		</div>
		</div>
	</div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
	<p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">

	<tr class="table-color-header">

		<th>No</th>
		<th>Kode Invoice</th>
		<th>Nama member</th>
		<th>Nama Outlet</th>
		<th>Paket</th>
		<th>Harga</th>
		<th>tgl</th>
		<th>Status</th>
		<th>Pembayaran</th>
		<th>Pelayan</th>
		<th width="280px">Action</th>
	</tr>
	@foreach ($transaksi as $transaksis)

	<tr>
		<td>{{ ++$i }}</td>
		<td>{{ $transaksis->kode_invoice }}</td>
		<td>{{ $transaksis->member->nama ?? ''}}</td>
		<td>{{ $transaksis->outlet->nama_outlet }}</td>
		<td>{{ $transaksis->paket->nama_paket }}</td>
		<td>{{ $transaksis->paket->harga }}</td>
		<td>{{ $transaksis->tgl }}</td>
		<td>{{ $transaksis->status }}</td>
		@if($transaksis->dibayar == '1')
		<td>Sudah Dibayar</td>
		@else
		<td>Belum Dibayar</td>
		@endif
		<td>{{ $transaksis->user->nama_user }}</td>
	
	<td>
		<form class="center" action="{{ route('transaksi.destroy',$transaksis->id_transaksi) }}" method="POST">

			<a class="btn btn-primary" href="{{ route('transaksi.edit', $transaksis->id_transaksi) }}" >Edit</a>

			@csrf
			@method('DELETE')

			<button type="submit" class="btn btn-danger">Delete</button>

			

		</form>
		
	</td>

	</tr>

	@endforeach

</table>

{!! $transaksi->links() !!}

</div>

@endsection
