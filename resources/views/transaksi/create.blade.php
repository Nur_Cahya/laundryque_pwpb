@extends('layouts.app')

@section('content')

<div class="container">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

<div class="row">
	<div class="col-lg-12 margin-tb">

		<div class="col-sm-12 float-left">
			
			<a class="btn btn-primary" href="{{ URL::previous() }}" > Back</a>

		</div>
		
		
		<div class="text-center txt-tittle">
			
			<h2>Entry transaksi</h2>

		</div>

	</div>


</div>
<div class="text-center txt-outlet">
	<div class="col-xm-12">
		@foreach($outlet->where('id_outlet',auth()->user()->id_outlet) as $toko)
		<h1>" {{ $toko->nama_outlet }} "</h1>
		@endforeach
	</div>

</div>

@if ($errors->any())



<div class="alert alert-danger">
	
	<strong>Upss! </strong> Ada yang salah dari inputanmu. <br><br>

	<ul>
		
		@foreach ($errors->all() as $error)

		<li>{{ $error }}</li>

		@endforeach

	</ul>

</div>

@endif



<form action="{{ route('transaksi.store') }}" method="POST">
	
	@csrf

	<input type="text" class="none" id="kode_invoice" name="kode_invoice" value="{{$invoice}}">
	<input type="text" class="none" id="tgl" name="tgl" value="{{$tgl->toDateString()}}">
	<input type="text" class="none" id="status" name="status" value="{{$status}}">
	<input type="text" class="none" id="id_user" name="id_user" value="{{$user}}">
	<input type="text" class="none" id="id_outlet" name="id_outlet" value="{{ auth()->user()->id_outlet }}">



	<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-11">
			
			<div class="form-group">

				<div class="col-sm-12 txt-nama">
				
				<h2>Nama Member :</h2>
				</div>

				<div class="btn-newmem">
					<a href="{{ route('member.create') }}" class="btn btn-info mb-3" style="color: #fff">New Member</a>
				</div>

				<select name="id_member" id="id_member" class="form-control name-form">
					@foreach($member as $m)
					<option value="{{$m->id_member}}" >{{ $m->nama }}</option>
					@endforeach
				</select>
				

			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-11">
			
			<div class="form-group">

				<div class="col-sm-12 txt-nama">
				
				<h2>Paket :</h2>

				</div>

				<select name="id_paket" id="id_paket" class="form-control name-form">
					@foreach($paket->where('id_outlet', auth()->user()->id_outlet) as $o)
					<option value="{{ $o->id_paket }}">{{ $o->nama_paket }}</option>
					@endforeach
				</select>

			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-11">
			
			<div class="form-group">

				<div class="col-sm-12 txt-nama">
				
				<h2>Status Pembayaran :</h2>

				</div>

				<select name="dibayar" id="dibayar" class="form-control name-form">
					<option value="1">Dibayar</option>
					<option value="0">Belum Dibayar</option>
				</select>

			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-11 text-center">

			<button type="submit" class="btn btn-primary btn-submit mt-3">Entry Transaksi</button>


		</div>

	</div>

</form>

</div>
@endsection