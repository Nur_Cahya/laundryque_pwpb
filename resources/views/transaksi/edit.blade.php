@extends('layouts.app')

@section('content')

<div class="container">
		<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

<div class="row">
	<div class="col-lg-12 margin-tb">

		<div class="col-sm-12 float-left mt-3">
			
			<a class="btn btn-primary" href="{{ URL::previous() }}" > Back</a>

		</div>
		
		
		<div class="text-center txt-tittle">
			
			<h2>Entry transaksi</h2>

		</div>

	</div>

</div>
<div class="text-center txt-outlet">
	<div class="col-xm-12">
		@foreach($outlet->where('id_outlet',auth()->user()->id_outlet) as $toko)
		<h1>" {{ $toko->nama_outlet }} "</h1>
		@endforeach
	</div>
</div>

@if ($errors->any())

<div class="alert alert-danger">
	
	<strong>Upss! </strong> Ada yang salah dari inputanmu. <br><br>

	<ul>
		
		@foreach ($errors->all() as $error)

		<li>{{ $error }}</li>

		@endforeach

	</ul>

</div>

@endif

<form action="{{ route('transaksi.update',$transaksi->id_transaksi) }}" method="POST">
	
	@csrf
	@method('PUT')

	<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-12">
			
			<div class="form-group">

				<div class="col-sm-12 txt-nama">
				
				<h2>Nama Member :</h2>

			</div>

			<div class="edit-txt-name">
				
				<h2 type="text" id="id_member" name="id_member" value="" class="">{{ $transaksi->member->nama ?? '' }}</h2>
				</div>

			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-12">
			
			<div class="col-sm-12 txt-nama">
				
				<h2>Paket 	:</h2>

				<div class="edit-txt-paket">

					<h2 type="text" id="id_paket" name="id_paket" value="" >{{ $transaksi->paket->nama_paket }} </h2>
				</div>
			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-11">
			
			<div class="col-sm-12 txt-nama mt-2">
				
				<h2>Status Pembayaran :</h2>

				<select name="dibayar" id="dibayar" class="form-control">
					<option value="1">Dibayar</option>
					<option value="0">Belum dibayar</option>
				</select>

			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-11">
			
			<div class="col-sm-12 txt-nama mt-4">
				
				<h2>Status Pengerjaan :</h2>

				<select name="status" id="status" class="form-control">
					<option value="baru">Baru</option>
					<option value="selesai">Selesai</option>
					<option value="diambil">Diambil</option>
				</select>

			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-11 text-center">

			<button type="submit" class="btn btn-primary mt-5 btn-submit">Edit Transaksi</button>


		</div>

	</div>

</form>

</div>
@endsection