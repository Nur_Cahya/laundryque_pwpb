@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
             <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

                <div class="card-body card-register">
                    <form method="POST" action="{{ route('member.store') }}">
                        @csrf
                        <div class="row text-center">
                            <div class="col-sm-12 tittle">
                                <h1>REGISTER MEMBER</h1>
                                <div class="underline-tittle-regis"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user-name" class="label-regis" >&nbsp;Nama</label>
                            <div class="col-md-8 mt-3 form-nama">
                                <input id="nama" type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" required autocomplete="name" placeholder="Masukan Nama" autofocus>

                                @error('nama')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user-name" class="label-regis" >&nbsp;Alamat</label>
                            <div class="col-md-8 mt-3 form-alamat">
                                <input id="alamat" type="alamat" class="form-control @error('alamat') is-invalid @enderror" name="alamat" value="{{ old('alamat') }}" required autocomplete="alamat" placeholder="Masukan alamat">

                                @error('alamat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user-name" class="label-regis" >&nbsp;no_telp</label>

                            <div class="col-md-8 mt-3 form-no_tlp">
                                <input id="no_telp" type="no_telp" class="form-control @error('no_telp') is-invalid @enderror" name="no_telp" required autocomplete="new-no_telp" placeholder="Masukan no_telp">

                                @error('no_telp')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user-name" class="label-regis" >&nbsp;Jenis Kelamin</label>

                            <div class="col-md-8 mt-3">
                                <select name="jk" id="jk" class="form-control">
                                	<option value="Laki-Laki">Laki-Laki</option>
                                	<option value="Perempuan">Perempuan</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row mt-2">
                            <div class="col-md-12 ">
                                <button type="submit" class="btn btn-primary btn-regis">
                                    {{ __('R E G I S T E R') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
