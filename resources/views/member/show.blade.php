@extends('layouts.app')

@section('content')
<div class="body">
	<div class="container">
		<div class="row justify-content-center">
			<div>
              
                <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

                <div class="card-body card-member">
                        @csrf
                        <div class="row text-center">
                            <div class="col-sm-12 tittle">
                                <h1>Data Member</h1>
                                <div class="underline-tittle-show-pengguna"></div>
                            </div>
                        </div>

                 <div class="row">
	
					<div class="col-xs-12 col-sm-12 col-md-12 mt-4">
		
						<div class="form-group name-txt">
							<h2>Nama Member :</h2>

							<div class="txt-datmem">
							<h2>{{ $member->nama }}</h2>

							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-12 col-md-12 mt-2">
		
						<div class="form-group name-txt">
							<h2>Alamat 		:</h2>

							<div class="txt-datmem">
							<h2>{{ $member->alamat }}</h2>

							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-12 col-md-12 mt-2">
		
						<div class="form-group name-txt">
							<h2>Nomor Telepon 		:</h2>

							<div class="txt-datmem">
							<h2>{{ $member->no_telp }}</h2>

							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-12 col-md-12 mt-2">
		
						<div class="form-group name-txt">
							<h2>Jenis Kelamin 		:</h2>

							<div class="txt-datmem">
							<h2>{{ $member->jk }}</h2>

						</div>
					</div>
				</div>
					  <div class="form-group row mt-2">
                            <div class="col-md-12">
                              <a class="btn btn-primary btn-back" href="{{ route('member.index')}}">B A C K</a>

                            </div>
                        </div>


			</div>
		</div>
	</div>
</div>



@endsection