@extends('layouts.app')

@section('content')

<div class="container-fluid">

	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">	

<div class="row">
	<div class="col-lg-12 margin-tb">
		<div class="pull-left">
			<div class="title">

			<h2>Data Member</h2>
		</div>
		<div class="pull-right">
			<a class="btn btn-success" href="{{ route('member.create') }}"> Daftarkan Member Baru</a>
		</div>
		</div>
	</div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
	<p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">

	<tr class="table-color-header">

		<th>No</th>
		<th>Nama</th>
		<th>Alamat</th>
		<th>No Telepon</th>
		<th>Jenis Kelamin</th>
		<th width="280px">Action</th>
	</tr>
	@foreach ($member as $members)

	<tr>
		<td>{{ ++$i }}</td>
		<td>{{ $members->nama }}</td>
		<td>{{ $members->alamat }}</td>
		<td>{{ $members->no_telp }}</td>
		<td>{{ $members->jk }}</td>
	
	<td>
		<form class="center" action="{{ route('member.destroy',$members->id_member) }}" method="POST">

			<a class="btn btn-success" href="{{ route('member.show', $members->id_member) }}" >Show</a>


			@csrf
			@method('DELETE')

			<button type="submit" class="btn btn-danger">Delete</button>

			

		</form>
		
	</td>

	</tr>

	@endforeach

</table>

{!! $member->links() !!}

</div>

@endsection
