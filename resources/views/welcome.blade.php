<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    
        
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laundryque</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

        <!-- Styles -->

        <style>
            html, body {
                /*background-color: #1e90ff;*/
                background-image: linear-gradient(to right, #1e90ff , #0fbcf9);
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                padding: 0;
            }
            .bubbles{
                position:absolute;
                width: 100%;
                height: 100%;
                z-index: 0;
                overflow: hidden;
                top: 0;
                left: 0;
            }
            .bubble{
                position: absolute;
                bottom: 0;
                /*background: #ffffff;*/
                background-image: linear-gradient(to bottom, #ffffff , #0abde3);
                border-radius: 50%; 
                opacity: 0.5;
                animation: flying 10s infinite ease-in;
                box-shadow: 1px 2px 5px rgba(0, 0, 0, 0.65);

            }

            .bubble:nth-child(1){
                width: 40px;
                height: 40px;
                left: 10%;
                animation-duration: 8s;   
            }

            .bubble:nth-child(2){
                width: 20px;
                height: 20px;
                left: 20%;
                animation-duration: 5s;
                animation-delay: 1s;   
            }

            .bubble:nth-child(3){
                width: 50px;
                height: 50px;
                left: 35%;
                animation-duration: 10s;
                animation-delay: 2s;   
            }

             .bubble:nth-child(4){
                width: 80px;
                height: 80px;
                left: 50%;
                animation-duration: 7s;
                animation-delay: 0s;   
            }

            .bubble:nth-child(5){
                width: 35px;
                height: 35px;
                left: 55%;
                animation-duration: 6s;
                animation-delay: 2s;   
            }

            .bubble:nth-child(6){
                width: 45px;
                height: 45px;
                left: 65%;
                animation-duration: 8s;
                animation-delay: 3s;   
            }

            
            .bubble:nth-child(7){
                width: 25px;
                height: 25px;
                left: 75%;
                animation-duration: 7s;
                animation-delay: 2s;   
            }

            .bubble:nth-child(8){
                width: 80px;
                height: 80px;
                left: 80%;
                animation-duration:6s;
                animation-delay: 1s;   
            }

            .bubble:nth-child(9){
                width: 15px;
                height: 15px;
                left: 70%;
                animation-duration:9s;
                animation-delay: 0s;   
            }

            .bubble:nth-child(10){
                width: 50px;
                height: 50px;
                left: 85%;
                animation-duration:5s;
                animation-delay: 3s;   
            }
            

            @keyframes flying{
                0%{
                    bottom: -100px;
                    transform: translateX(0);
                }
                50%{
                    transform: translateX(100px);
                }
                100%{
                    bottom: 1080px;
                    transform: translateX(-200px);
                }

            }

            .judul{
                font-family: "montserrat",sans-serif;

            }

            .judul h1{
                font-size: 75px;
                text-transform: uppercase;
                font-weight: 900;
                color: #2c3e5035;
            }
            .judul{
                background-image: url("../img/water2.png");
                -webkit-background-clip:text;
                animation: aer 15s infinite;
            }

            @keyframes aer{
                from{
                    background-position: left 0 top 0 ;
                }
                to{
                    background-position: left 1000px top 0;
                }
            }


            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
                position: absolute;
            }

            .title {
                font-size: 84px;
            }

            .sub-content{
                font-size: 12px;
                color: #fff
            }

            .decs h2 {
                text-shadow: 2px 2px 5px #4b7bec;
                font-family: "Raleway", sans-serif;
                font-size: 16px;

            }

            .links > a {
                color: #fff;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="bubbles">
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        <div class="bubble"></div>
        </div>  

        <div class="flex-center position-ref full-height">
            <div class="text-link">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">HOME</a>
                    @else
                        <a href="{{ route('login') }}">LOGIN</a>
                    @endauth
                </div>
            </div>
        @endif
            
            <div class="content">
                <div class="title m-b-md">
                    <div class="judul">
                    <h1>Laundryque</h1>
                    </div>
                </div>
                <!-- <div class="bg"> -->

                <div class="sub-content">
                    <div class="decs">
                    <h2>Cucian kotor menumpuk? Di laundryque aja solusinya!</h2>
                    </div>
                </div>

            </div>
        </div>
         
    </body>
</html>
