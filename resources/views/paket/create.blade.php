@extends('layouts.app')

@section('content')

<div class="container">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

<div class="row">
	<div class="col-lg-12 margin-tb">
		
		<div class="text-center txt-tittle">
			
			<h2>Tambah paket Baru</h2>

		</div>

		<div class="pull-right">
			
			<a class="btn btn-primary" href="{{ URL::previous() }}" > Back</a>

		</div>

	</div>

</div>

@if ($errors->any())

<div class="alert alert-danger">
	
	<strong>Upss! </strong> Ada yang salah dari inputanmu. <br><br>

	<ul>
		
		@foreach ($errors->all() as $error)

		<li>{{ $error }}</li>

		@endforeach

	</ul>

</div>

@endif

<form action="{{ route('paket.store') }}" method="POST">
	
	@csrf
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 mt-3">
			<div class="form-group">

				<div class="txt-nama">
				
				<h2>Pilih Outlet :</h2>
			</div>
				<select name="id_outlet" id="" class="form-control">
					@foreach($outlet as $toko)
					<option value="{{ $toko->id_outlet }}">{{ $toko->nama_outlet }}</option>
					@endforeach
				</select>
			</div>
		</div>
	</div>
	<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-12">
			
			<div class="form-group">

				<div class="txt-nama">
				
				<h2>Nama paket :</h2>

			</div>

				<input type="text" name="nama_paket" class="form-control" placeholder="Masukan Nama paket/ Produk">

			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-12">
			
			<div class="form-group">

				<div class="txt-nama">
				
				<h2>Jenis Paket :</h2>

			</div>

				<input type="text" name="jenis" id="jenis" class="form-control" placeholder="Masukan Jenis Paket ex (Baju, Celana , Selimut dll)">

			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-12">

			<div class="form-group">
				<div class="txt-nama">

				<h2>Harga :</h2>

			</div>

				<input type="text" name="harga" class="form-control" placeholder="Masukan Harga" >

			</div>
			
		</div>

		<div class="col-xs-12 col-sm-12 col-md-12 text-center">

			<button type="submit" class="btn btn-primary btn-submit-paket">Tambah Paket</button>


		</div>

	</div>

</form>

</div>
@endsection