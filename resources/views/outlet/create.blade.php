@extends('layouts.app')

@section('content')

<div class="container">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

<div class="row">
	<div class="col-lg-12 margin-tb">
		
		<div class="text-center txt-tittle">
			
			<h2>Tambah Outlet Baru</h2>

		</div>

		<div class="pull-right">
			
			<a class="btn btn-primary" href="{{ route('outlets.index') }}" > Back</a>

		</div>

	</div>

</div>

@if ($errors->any())

<div class="alert alert-danger">
	
	<strong>Upss! </strong> Ada yang salah dari inputanmu. <br><br>

	<ul>
		
		@foreach ($errors->all() as $error)

		<li>{{ $error }}</li>

		@endforeach

	</ul>

</div>

@endif

<form action="{{ route('outlets.store') }}" method="POST">
	
	@csrf

	<div class="row">
		
		<div class="col-xs-12 col-sm-12 col-md-12 mt-4">
			
			<div class="form-group txt-nama">
				
				<h2>Nama Outlet :</h2>

				<input type="text" name="nama_outlet" class="form-control" placeholder="Masukan Nama Outlet">

			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-12">
			
			<div class="form-group txt-nama">
				
				<h2>Alamat :</h2>

				<textarea class="form-control" style="height:150px" name="alamat" placeholder="Masukan Alamat"></textarea>

			</div>

		</div>

		<div class="col-xs-12 col-sm-12 col-md-12">

			<div class="form-group txt-nama">
				<h2>No Telepon :</h2>

				<input type="text" name="no_telp" class="form-control" placeholder="Masukan No Telepon">

			</div>
			
		</div>

		<div class="col-xs-12 col-sm-12 col-md-12 text-center">

			<button type="submit" class="btn btn-primary btn-submit-edit">Tambah Outlet</button>


		</div>

	</div>

</form>

</div>
@endsection