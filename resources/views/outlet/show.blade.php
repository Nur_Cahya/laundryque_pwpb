@extends('layouts.app')

@section('content')
@if ($message = Session::get('success'))
<div class="alert alert-success">
	<p>{{ $message }}</p>
</div>
@endif
<div class="container-fluid">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

<div class="row">
	
	<div class="col-lg-12 margin-tb">
		
		<div class="text-center txt-tittle">
			
			<h2>Detail Outlet</h2>

		</div>

		<div class="float-left">
			
			<a class="btn btn-primary btn-back-detail-outlet" href="{{ route('outlets.index')}}">Back</a>

		</div>

	</div>
 
</div>

<div class="row row-left">
	
	<div class="col-xs-12 col-sm-12 col-md-6 mt-2">
		
		<div class="form-group txt-nama">
			
			<h2>Nama Outlet :</h2>

			<div class="txt-datmem-detout">

				<h2>{{ $outlet->nama_outlet }}</h2>
			</div>

		</div>

	</div>

	<div class="col-xs-12 col-sm-12 col-md-12">
		
		<div class="form-group txt-nama">
			
			<h2>Alamat :</h2>

			<div class="txt-datmem-detout">

				<h2>{{ $outlet->alamat }}</h2>

				</div>

		</div>

	</div>

	<div class="col-xs-12 col-sm-12 col-md-12">
		
		<div class="form-group txt-nama">
			
			<h2>No Telepon :</h2>

			<div class="txt-datmem-detout">

				<h2>{{ $outlet->no_telp }}</h2>
			</div>

		</div>

	</div>

</div>
</div>
<div class="container">
	<div class="row">
	<div class="col-lg-12 margin-tb">
		<div class="float-left">
			<div class="title">

			<h2>Data paket yang tersedia</h2>
		</div>
		<div class="float-left">
			<a class="btn btn-success" href="{{ route('paket.create') }}"> Buat paket Baru</a>
		</div>
		</div>
	</div>
</div>



<table class="table table-bordered">

	<tr class="table-color-header">

		<th>Jenis</th>
		<th>Nama Paket</th>
		<th>Harga</th>
		<th width="280px">Action</th>
	</tr>
	@foreach ($paket->where('id_outlet', $outlet->id_outlet) as $paketan)

	<tr>
		<td>{{ $paketan->jenis }}</td>
		<td>{{ $paketan->nama_paket }}</td>
		<td>{{ $paketan->harga }}</td>
	
	<td>
		<form class="center" action="{{ route('paket.destroy',$paketan->id_paket) }}" method="POST">

			<a class="btn btn-primary" href="{{ route('paket.edit', $paketan->id_paket) }}" >Edit</a>

			@csrf
			@method('DELETE')

			<button type="submit" class="btn btn-danger">Delete</button>

			

		</form>
		
	</td>

	</tr>

	@endforeach

</table>


</div>

@endsection