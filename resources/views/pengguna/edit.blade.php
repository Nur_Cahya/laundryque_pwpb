@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
             <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

                <div class="card-body card-register-edit">

                    <form method="POST" action="{{ route('pengguna.update',$user->id) }}">
                        @csrf
                        @method('PUT')
                        <div class="row text-center">
                            <div class="col-sm-12 tittle">
                                <h1>EDIT PENGGUNA</h1>
                                <div class="underline-tittle-edit-pengguna"></div>
                            </div>
                        </div>
                        <input type="text" class="none" name="id" value="{{ $user->id }}">
                        <div class="form-group row">
                            <label for="user-name" class="label-regis" >&nbsp;Nama</label>
                            <div class="col-md-8 mt-3 form-nama-edit">
                                <input id="nama_user" type="text" class="form-control @error('nama_user') is-invalid @enderror" name="nama_user" value="{{ $user->nama_user }}" required autocomplete="name" placeholder="Masukan Nama" autofocus>

                                @error('nama_user')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user-name" class="label-regis" >&nbsp;Email</label>
                            <div class="col-md-8 mt-3 form-email-edit">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email" placeholder="Masukan Email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="" class="label-regis">&nbsp;Role</label>

                            <div class="col-md-8 mt-3">
                                <select name="role" id="" class="form-control form-role-edit">
                                    <option value="">Pilih Role Akun</option>
                                    <option value="admin" {{ $user->isAdmin == 1 ? 'selected' : '' }}>Admin</option>
                                    <option value="owner" {{ $user->isOwner == 1 ? 'selected' : '' }}>Owner</option>
                                    <option value="kasir" {{ $user->isKasir == 1 ? 'selected' : '' }}>Kasir</option>
                                </select>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="" class="label-regis">&nbsp;Outlet</label>

                            <div class="col-md-8 mt-3">
                                <select name="id_outlet" id="" class="form-control form-outlet-edit">
                                    @foreach($outlet->where('id_outlet', $user->id_outlet) as $o)
                                    <option value="{{$o->id_outlet}}">{{$o->nama_outlet}}</option>
                                    @endforeach
                                    <div class="divider"></div>
                                    @foreach($outlet as $o)
                                    <option value="{{$o->id_outlet}}">{{$o->nama_outlet}}</option>
                                    @endforeach
                                    
                                </select>
                            </div>

                        </div>

                        <div class="form-group row mt-2">
                            <div class="col-md-12 ">
                                <button type="submit" class="btn btn-primary btn-regis">
                                    {{ __('E D I T') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
