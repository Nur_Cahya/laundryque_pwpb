@extends('layouts.app')

@section('content')

<div class="container-fluid">	
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

<div class="row">
	<div class="col-lg-12 margin-tb">
		<div class="pull-left">
			<div class="title">

			<h2>Data pengguna</h2>
		</div>
		<div class="pull-right">
			<a class="btn btn-success" href="{{ route('pengguna.create') }}"> Buat pengguna Baru</a>
		</div>
		</div>
	</div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
	<p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">

	<tr class="table-color-header">

		<th>No</th>
		<th>Username</th>
		<th>Email</th>
		<th>Nama Outlet</th>
		<th>Role</th>
		<th width="280px">Action</th>
	</tr>
	@foreach ($user as $users)

	<tr>
		<td>{{ ++$i }}</td>
		<td>{{ $users->nama_user }}</td>
		<td>{{ $users->email }}</td>
		<td>{{ $users->outlet->nama_outlet }}</td>
		@if($users->isAdmin == '1')
		<td>Admin</td>
		@elseif($users->isKasir == '1')
		<td>Kasir</td>
		@elseif($users->isOwner == '1')
		<td>Owner</td>
		@else
		<td>belum memiliki role</td>
		@endif
	
	<td>
		<form class="center" action="{{ route('pengguna.destroy',$users->id) }}" method="POST">

			<a class="btn btn-primary" href="{{ route('pengguna.edit', $users->id) }}" >Edit</a>

			@csrf
			@method('DELETE')

			<button type="submit" class="btn btn-danger">Delete</button>

			

		</form>
		
	</td>

	</tr>

	@endforeach

</table>

{!! $user->links() !!}

</div>

@endsection
