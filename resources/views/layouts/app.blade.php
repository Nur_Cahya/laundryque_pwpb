<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laundryque') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
    <div id="app">
        <nav class="navbar navbarku navbar-expand-md navbar-dark shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    Laundryque
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('LOGIN') }}</a>
                            </li>
                        @else
                            @if (auth()->user()->isAdmin == 1)
                                <li class="nav-item">
                                    <a href="" class="nav-link">Generate Laporan</a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('transaksi.create') }}" class="nav-link">Entry Transaksi</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href=" "></a>
                                    <li class="nav-item dropdown">
                                        <a href="" class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>CRUD <span class="caret"></span></a>

                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                            <a href="{{ route('outlets.index') }}" class="dropdown-item">
                                                Data Outlet
                                            </a>
                                            <a href="{{ route('member.index') }}" class="dropdown-item">
                                                Data Member
                                            </a>
                                            <a href="{{ route('pengguna.index') }}" class="dropdown-item">
                                                Data Pengguna
                                            </a>
                                            <a href="{{ route('transaksi.index') }}" class="dropdown-item">
                                                Transaksi
                                            </a>
                                        </div>

                                    </li>
                                </li>
                            @elseif (auth()->user()->isKasir == 1)
                                <li class="nav-item">
                                    <a href="" class="nav-link">Generate Laporan</a>
                                </li>   
                                <li class="nav-item">
                                        <a href="{{ route('member.index') }}" class="nav-link">
                                            Member
                                        </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('transaksi.index') }}" class="nav-link">
                                        Transaksi
                                    </a>
                                </li>
                            @elseif (auth()->user()->isOwner == 1)
                                <li class="nav-item">
                                    <a href="" class="nav-link">Generate Laporan</a>
                                </li>
                            @else
                                  
                            @endif
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Profile <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                    <a class="dropdown-item" href="">
                                        {{ Auth::user()->nama_user }}
                                    </a>
                                    
                                    <div class="dropdown-divider"></div>

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
