@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
             <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

                <div class="card-body card-tambah-pengguna">
                    <form method="POST" action="{{ route('pengguna.store') }}">
                        @csrf
                        <div class="row text-center">
                            <div class="col-sm-12 tittle">
                                <h1>REGISTER PENGGUNA</h1>
                                <div class="underline-tittle-tambah-pengguna"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user-name" class="label-regis" >&nbsp;Nama</label>
                            <div class="col-md-8 mt-3 form-nama-tambah">
                                <input id="nama_user" type="text" class="form-control @error('nama_user') is-invalid @enderror" name="nama_user" value="{{ old('nama_user') }}" required autocomplete="name" placeholder="Masukan Nama" autofocus>

                                @error('nama_user')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user-name" class="label-regis" >&nbsp;Email</label>
                            <div class="col-md-8 mt-3 form-email-tambah">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Masukan Email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user-name" class="label-regis" >&nbsp;Password</label>

                            <div class="col-md-8 mt-3 form-pass-tambah">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Masukan Password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user-name" class="label-regis" >&nbsp;Confirm Password</label>

                            <div class="col-md-8 mt-3">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Masukan Password Kembali">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="label-regis">&nbsp;Role</label>

                            <div class="col-md-8 mt-3 offset-2">
                                <select name="role" id="" class="form-control form-role-tambah">
                                    <option value="admin">Admin</option>
                                    <option value="owner">Owner</option>
                                    <option value="kasir">Kasir</option>
                                </select>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="" class="label-regis">&nbsp;Outlet</label>

                            <div class="col-md-8 mt-3 offset-2">
                                <select name="id_outlet" id="" class="form-control">
                                    @foreach($outlet as $o)
                                    <option value="{{$o->id_outlet}}">{{$o->nama_outlet}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                        <div class="form-group row mt-2">
                            <div class="col-md-12 ">
                                <button type="submit" class="btn btn-primary btn-regis">
                                    {{ __('R E G I S T E R') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
