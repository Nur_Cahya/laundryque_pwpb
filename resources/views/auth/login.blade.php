@extends('layouts.app')

@section('content')
<div class="body">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div>
              
                <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

                <div class="card-body card-login ">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="row text-center">
                            <div class="col-sm-12 tittle">
                                <h1>LOGIN</h1>
                                <div class="underline-tittle"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                                
                         

                            <div class="col-md-12 mt-3">
                                <label for="user-email" class="label" >&nbsp;Email</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder=" Masukan Email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row ">

                            <div class="col-md-12 mt-3">
                                <label for="user-pass" class="label">&nbsp;Password</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required placeholder="Masukan Password" autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                        </div>


                        <div class="form-group row mt-2">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-login">
                                    {{ __('L O G I N') }}
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div></div>
@endsection
